import React from "react";
import {Alert, Dimensions, PermissionsAndroid, Share} from 'react-native';
import colors from './src/assets/colors';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from './src/responsive-screen/index';
import * as BaseSize from './src/assets/Size/Size';
import ButtonComponent from './src/components/button/ButtonComponent';
import TextComponent from './src/components/text/text';
import TextInputArea from './src/components/textInput/TextInputArea'
import {formatVND as formatVND, validateText as validateText} from './src/baseFunctions/baseFunctions';
import StarRating from './src/components/star-rating/index';
import RadioGroup from './src/components/radio-button/radioGroup';
import RadioButton from './src/components/radio-button/radioButton';
import ButtonOutline from "./src/components/button/ButtonOutline";
import SaveCancelButton from "./src/components/button/SaveCancelButton";
import TwoButton from "./src/components/button/TwoButton";
import Kaede from './src/components/textInput/Kaede';
import Hoshi from './src/components/textInput/Hoshi';
import Jiro from './src/components/textInput/Jiro';
import Isao from './src/components/textInput/Isao';
import Madoka from './src/components/textInput/Madoka';
import Akira from './src/components/textInput/Akira';
import Hideo from './src/components/textInput/Hideo';
import Kohana from './src/components/textInput/Kohana';
import Makiko from './src/components/textInput/Makiko';
import Sae from './src/components/textInput/Sae';
import Fumi from './src/components/textInput/Fumi';
export {
    colors,
    wp,
    hp,
    BaseSize,
    ButtonComponent,
    TextComponent,
    TextInputArea,
    formatVND,
    validateText,
    StarRating,
    RadioButton,
    RadioGroup,
    ButtonOutline,
    SaveCancelButton,
    TwoButton,
    Kaede,
    Hoshi,
     Jiro,
     Isao,
     Madoka,
     Akira,
     Hideo,
     Kohana,
     Makiko,
     Sae,
     Fumi,
}
