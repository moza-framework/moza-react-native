import {NavigationActions, StackActions} from "react-navigation";
import {RNToasty} from "react-native-toasty";

const formatVND = (num) => {
    if (validateText(num)) {
        return String(num).replace(/(.)(?=(\d{3})+$)/g, '$1,');
    }
};
const validateText = (text) => {
    return text !== '' && text !== null
        && text !== undefined && text !== 'null' &&
        text !== 'undefined'
};
const navigateFinishPreScreen = (screen, navigation) => {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({routeName: screen})
        ]
    });
    navigation.dispatch(resetAction);
};

const inValidateText = (text) => {
    return text === '' || text === null ||
        text === 'null' || text === 'undefined' ||
        text === undefined
};
const ToastyError = (title) => {
    return RNToasty.Error({
        title: title
    });
};

const ToastyErrorCustom = (title, duration) => {
    return RNToasty.Error({
        title: title,
        duration: duration,
    });
};

const ToastySuccess = (title) => {
    return RNToasty.Success({
        title: title,
    });
};

const ToastySuccessCustom = (title, duration) => {
    return RNToasty.Success({
        title: title,
        duration: duration
    });
};

const ToastyWarning = (title) => {
    return RNToasty.Warn({
        title: title
    });
};

const ToastyWarningCustom = (title, duration) => {
    return RNToasty.Warn({
        title: title,
        duration: duration
    });
};

const ToastyInfo = (title) => {
    return RNToasty.Info({
        title: title
    });
};

const ToastyInfoCustom = (title, duration) => {
    return RNToasty.Info({
        title: title,
        duration: duration
    });
};

export {formatVND, validateText}
