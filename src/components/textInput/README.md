# React Native Textinput Effects

![](screenshots/full.gif)

## Installation


You also need to install [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons) if you'd like to use a TextInput component with an icon. Please check out [Installation section](https://github.com/oblador/react-native-vector-icons#installation) on that project.

## Examle

- [TextInput](https://gitlab.com/moza-framework/moza-react-native/blob/master/src/components/textInput/Example/index.js)


| Prop | Type | Description |
|---|---|---|
|**`label`**|String|Displayed as placeholder string of the input.|
|**`style`**|View Style Object|Applied to the root container of the input.|
|**`labelStyle`**|View Style Object|Applied to the container of the label view.|
|**`inputStyle`**|Text Style Object|Applied to the TextInput component.|
|**`value`**|String|This value will be applied to the TextInput and change it's state on every render. Use this prop if you want a [Controlled Component](https://facebook.github.io/react/docs/forms.html#controlled-components).|
|**`defaultValue`**|String|If you want to initialize the component with a non-empty value, you can supply a defaultValue prop. This prop creates an [Uncontrolled Component](https://facebook.github.io/react/docs/forms.html#uncontrolled-components) and is only used during initial render.|

You can also use default [TextInput Props](https://facebook.github.io/react-native/docs/textinput.html#props). They'll be passed into TextInput component. E.g., use `TextInput`'s `onChange` prop to be notified on text changes.
```js
<Sae
  onChangeText={(text) => { this.setState({textValue: text}) }
/>
```

### Props for TextInputEffects with an Icon

This component needs `Icon` component from `react-native-vector-icons` to operate with icons. You should import it before creating a TextInputEffects component.

`import Icon from 'react-native-vector-icons/FontAwesome';`

| Prop | Type | Description |
|---|---|---|
|**`iconClass`**|Object|The Icon component class you've imported from react-native-vector-icons.|
|**`iconName`**|String|Name of the icon that is passed to Icon component.|
|**`iconColor`**|String|Applied to the Icon component.|
|**`iconSize`**|Number|Applied to the Icon component.|




### Sae

![](screenshots/sae.gif)


```js
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Sae } from 'moza-framework';

const saeInput = (
  <Sae
    label={'Email Address'}
    iconClass={FontAwesomeIcon}
    iconName={'pencil'}
    iconColor={'white'}
    inputPadding={16}
    labelHeight={24}
    // active border height
    borderHeight={2}
    // TextInput props
    autoCapitalize={'none'}
    autoCorrect={false}
  />
);
```

### Fumi

![](screenshots/fumi.gif)


```js
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Fumi } from 'moza-framework';

const fumiInput = (
  <Fumi
    label={'Course Name'}
    iconClass={FontAwesomeIcon}
    iconName={'university'}
    iconColor={'#f95a25'}
    iconSize={20}
    iconWidth={40}
    inputPadding={16}
  />
);
```
### Kohana

![](screenshots/kohana.gif)

Kohana supports [Animated Native Driver](https://facebook.github.io/react-native/docs/animations.html#using-the-native-driver). You can use native driver by passing `useNativeDriver`.

```js
import MaterialsIcon from 'react-native-vector-icons/MaterialIcons';
import { Kohana } from 'moza-framework';

const kohanaInput = (
  <Kohana
    style={{ backgroundColor: '#f9f5ed' }}
    label={'Line'}
    iconClass={MaterialsIcon}
    iconName={'directions-bus'}
    iconColor={'#f4d29a'}
    inputPadding={16}
    labelStyle={{ color: '#91627b' }}
    inputStyle={{ color: '#91627b' }}
    labelContainerStyle={{ padding: 20 }}
    iconContainerStyle={{ padding: 20 }}
    useNativeDriver
  />
);
```

### Makiko

![](screenshots/makiko.gif)


```js
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Makiko } from 'moza-framework';

const makikoInput = (
  <Makiko
    label={'Comment'}
    iconClass={FontAwesomeIcon}
    iconName={'comment'}
    iconColor={'white'}
    inputPadding={16}
    inputStyle={{ color: '#db786d' }}
  />
);
```
Note: Icon component expands and covers the input. So, the icon should not have any blank spaces for the animation experience. This is the limitation for Makiko.

### Isao

![](screenshots/isao.gif)


```js
import { Isao } from 'moza-framework';

const isaoInput = (
  <Isao
    label={'First Name'}
    // this is applied as active border and label color
    activeColor={'#da7071'}
    // active border height
    borderHeight={8}
    inputPadding={16}
    labelHeight={24}
    // this is applied as passive border and label color
    passiveColor={'#dadada'}
  />
);
```

### Hoshi

![](screenshots/hoshi.gif)


```js
import { Hoshi } from 'moza-framework';

const hoshiInput = (
  <Hoshi
    label={'Town'}
    // this is used as active border color
    borderColor={'#b76c94'}
    // active border height
    borderHeight={3}
    inputPadding={16}
    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
    backgroundColor={'#F9F7F6'}
  />
);
```

### Jiro

![](screenshots/jiro.gif)


```js
import { Jiro } from 'moza-framework';

const jiroInput = (
  <Jiro
    label={'Dog\'s name'}
    // this is used as active and passive border color
    borderColor={'#9b537a'}
    inputPadding={16}
    inputStyle={{ color: 'white' }}
  />
);
```

### Kaede

![](screenshots/kaede.gif)


```js
import { Kaede } from 'moza-framework';

const kaedeInput = (
  <Kaede
    label={'Website'}
    inputPadding={16}
  />
);
```

### Akira

![](screenshots/akira.gif)


```js
import { Akira } from 'moza-framework';

const akiraInput = (
  <Akira
    label={'First Name'}
    // this is used as active and passive border color
    borderColor={'#a5d1cc'}
    inputPadding={16}
    labelHeight={24}
    labelStyle={{ color: '#ac83c4' }}
  />
);
```

### Madoka

![](screenshots/madoka.gif)


```js
import { Madoka } from 'moza-framework';

const madokaInput = (
  <Madoka
    label={'Frequency'}
    // this is used as active and passive border color
    borderColor={'#aee2c9'}
    inputPadding={16}
    labelHeight={24}
    labelStyle={{ color: '#008445' }}
    inputStyle={{ color: '#f4a197' }}
  />
);
```

### Hideo

![](screenshots/hideo.gif)


```js
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Hideo } from 'moza-framework';

const hideoInput = (
  <Hideo
    iconClass={FontAwesomeIcon}
    iconName={'envelope'}
    iconColor={'white'}
    // this is used as backgroundColor of icon container view.
    iconBackgroundColor={'#f2a59d'}
    inputStyle={{ color: '#464949' }}
  />
);
```
