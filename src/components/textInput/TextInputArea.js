import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from '../../responsive-screen/index';
import colors from '../../assets/colors'

export default class TextInputArea extends Component {
    render() {
        return (
            <TextInput
                style={[styles.textArea,this.props.style]}
                underlineColorAndroid="transparent"
                placeholderTextColor={colors.lightgrey}
                numberOfLines={1}
                placeholder={this.props.placeholder}
                multiline={true}
                onChangeText={this.props.onChangeText}
                value={this.props.value}
                returnKeyType="next"
            />
        );
    }
}
const styles = StyleSheet.create({
    textArea: {
        width: wp('80%'),
        borderColor: colors.lightgrey,
        height: null,
        borderRadius: 5,
        borderBottomWidth: 0.5,
        padding: 4,
        justifyContent: "flex-start",
        fontWeight: '400',
        fontSize: 16,
    },
});
