import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from '../../responsive-screen/index';
import colors from '../../assets/colors'
export default class ButtonComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={[styles.container, this.props.style]}>
                <Text style={[styles.text,this.props.styleText]}>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        width: wp('80%'),
        height: 50,
        backgroundColor: colors.orange,
        alignItems: 'center',
        justifyContent: 'center',

    },
    text: {
        fontSize: 16,
        color: colors.white,
        fontWeight: '500'
    }
});
