# Moza Framework

install
```
$ yarn add https://gitlab.com/moza-framework/moza-react-native.git
 ```
update
```
$ yarn add moza-framework
```
remove
```
$ yarn remove moza-framework
```
- [Radio Button](https://gitlab.com/moza-framework/moza-react-native/blob/master/src/components/radio-button/README.md)

- [Start Rating](https://gitlab.com/moza-framework/moza-react-native/tree/master/src/components/star-rating)

Contributing
You can send how many PR's do you want, I'll be glad to analyse and accept them! And if you have any question about the project...

Email-me: mozasolution@gmail.com
```
Phone: +84912738748
```
Web: http://www.mozagroup.com/

Thank you
